import os


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """
    t1w        = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T1w')
    t2w        = create_key('sub-{subject}/{session}/anat/sub-{subject}_{session}_T2w')
    func_fam   = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-fam_bold')
    func_task  = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-exp_bold')
    func_rest  = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-rest_run-{item:01d}_bold')
    fmap_mag   = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_magnitude')
    fmap_phase = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_phasediff')
    dwi        = create_key('sub-{subject}/{session}/dwi/sub-{subject}_{session}_dwi')
    dwi_deriv  = create_key(('derivatives/scanner/sub-{subject}/{session}/dwi'
                        '/sub-{subject}_{session}_run-{item:01d}_TRACEW'))
    # dwi_b0s  = create_key('sub-{subject}/{session}/dwi/sub-{subject}_{session}_acq-B0s_dwi')
    # data = create_key('run{item:03d}')
    info = {t1w: [], t2w: [], func_fam: [], func_task: [], func_rest: [], fmap_mag: [], fmap_phase:[], dwi: [], dwi_deriv:[]}
    # last_run = len(seqinfo)

    for s in seqinfo:
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """
	# define T1w
        if (s.dim1 == 320) and (s.dim2 == 320) and ('t1_mpr' in s.protocol_name):
            info[t1w] = [s.series_id]
	# define T2w
        if (s.dim1 == 448) and (s.dim2 == 448) and ('t2_tse' in s.protocol_name):
            info[t2w] = [s.series_id]
	# define functional familiarization 
        if (s.dim1 == 72) and (s.dim4 > 300) and (s.dim4 > 180)and ('fmri' in s.protocol_name):
            info[func_fam] = [s.series_id]
	# define functional task
        if (s.dim1 == 72) and (s.dim4 > 495) and ('fmri' in s.protocol_name):
            info[func_task] = [s.series_id]
	# define functional resting
        if (s.dim1 == 72) and (s.dim2 == 84) and ('resting' in s.protocol_name):
            info[func_rest].append(s.series_id)
	# define field map (magnitude)
        if (s.dim3 == 100) and (s.dim4 == 1) and ('gre_field_mapping' in s.protocol_name):
            info[fmap_mag] = [s.series_id]
	# define field map (phasic)
        if (s.dim3 == 50) and (s.dim4 == 1) and ('gre_field_mapping' in s.protocol_name):
            info[fmap_phase] = [s.series_id]
	# define DTI
        if (s.dim1 == 128) and (s.dim3 == 4680) and ('diff' in s.protocol_name):
            info[dwi] = [s.series_id]
        # define DTI (baseline)
        if (s.dim1 == 128) and (s.dim3 == 72) and ('diff' in s.protocol_name):
            info[dwi_deriv] = [s.series_id]

    return info
