import os


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

    func_task  = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-exp_bold')
    func_rest  = create_key('sub-{subject}/{session}/func/sub-{subject}_{session}_task-rest_run-{item:01d}_bold')
    fmap_mag   = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_magnitude')
    fmap_phase = create_key('sub-{subject}/{session}/fmap/sub-{subject}_{session}_phasediff')
    info = {func_task: [], func_rest: [], fmap_mag: [], fmap_phase:[]}

    for s in seqinfo:
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """
	# define functional task (Note the dimension 4)
        if (s.dim1 == 72) and (s.dim4 > 600) and ('fmri' in s.protocol_name):
            info[func_task] = [s.series_id]
	# define functional resting
        if (s.dim1 == 72) and (s.dim2 == 84) and ('resting' in s.protocol_name):
            info[func_rest].append(s.series_id)
	# define field map (magnitude)
        if (s.dim3 == 100) and (s.dim4 == 1) and ('gre_field_mapping' in s.protocol_name):
            info[fmap_mag] = [s.series_id]
	# define field map (phasic)
        if (s.dim3 == 50) and (s.dim4 == 1) and ('gre_field_mapping' in s.protocol_name):
            info[fmap_phase] = [s.series_id]

    return info
