# Nipy version of script for RepDopa

This repo is for storing the Nipy scripts using in RepDopa project. This repo including three parts:
* scripts that use `Nipype_tutorial`(a container image) to reproduce previous analysis that used Matlab and SPM
* scripts that I learn [Dartbrain](https://dartbrains.org/intro.html)
* scripts that I used `Nipy` to reproduce the analyses that had been done using Matlab & SPM