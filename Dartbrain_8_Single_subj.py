import os
from glob import glob
import pandas as pd
import numpy as np

import nibabel as nib
from nilearn.plotting import view_img, glass_brain, plot_stat_map
from bids import BIDSLayout, BIDSValidator

from nltools.stats import zscore, regress, find_spikes
from nltools.data import Brain_Data, Design_Matrix
from nltools.file_reader import onsets_to_dm
from nltools.utils import get_resource_path

data_dir = '/home/hcp4715/Data/RepDopa/Nipy/BIDS'
layout = BIDSLayout(data_dir, derivatives=True)


tr = layout.get_tr()
fwhm = 6
spike_cutoff = 3

def recode_trial_type(trial_type):
    """ recode trial type based on needs
    Here, we hope to have a simple contrast for CS+ vs. CS-, therefore we pool all CS- and CS+ (with/without US)
    """
    if trial_type == 'CSplT' or trial_type == 'CSplF':
        return 'CSpl'
    elif trial_type == 'CSminT' or trial_type == 'CSminF':
        return 'CSmin'
    elif trial_type == 'RateCSmin' or trial_type == 'RateCSpl':
        return 'rating'
    elif trial_type == 'ContextOff' or trial_type == 'ContextOn':
        return 'context'

# define a function create design_matrix from BIDS event file
def load_bids_events(layout, subject, session):
    '''
    Create a design_matrix instance from BIDS events file.
    Note: the number of TR is same as the length of preprocessed data by fmriprep
    '''
    
    tr = layout.get_tr()
    n_tr = nib.load(layout.get(subject=subject, 
                               session=session,
                               scope='raw',
                               suffix='bold')[1].path).shape[-1] # Get number of TRs
    onsets = pd.read_csv(layout.get(subject=subject,
                                    session=session,
                                    scope='raw',
                                    suffix='events')[0].path, 
                         sep = '\t')
    onsets.onset = (onsets.onset - onsets.onset[0])/1000
    
    # Get the number of pulses (TRs)
    onsets['cumcount'] = onsets.groupby('trial_type').cumcount()
    # onsets.head()
    pulsenum = onsets[(onsets['trial_type'] == 'Pulse')]
    maxpulse = pulsenum['cumcount'].max()
    pulsenum.head()
    maxpulse

    cutoff_idx = onsets[(onsets['trial_type'] == 'Pulse') & (onsets['cumcount'] == maxpulse)].index.tolist()[0]
    # cutoff_idx


    onsets = onsets.truncate(before=0, after=cutoff_idx)
    
    #onsets = onsets[(onsets['onset']>0)]

    onsets['trial_type'] = onsets['trial_type'].apply(recode_trial_type)
    onsets = onsets[(onsets['trial_type'].notnull())]
    
    onsets = onsets.drop(['ratings', 'cumcount'], axis=1)

    onsets.rename(columns={'onset': 'Onset', 
     #              'duration': 'Duration',
                   'trial_type': 'Stim'}, inplace=True)
    
    return onsets_to_dm(onsets, sampling_freq = 1/tr, run_length=n_tr)

def make_motion_covariate(mc):
    z_mc = zscore(mc)
    all_mc = pd.concat([z_mc, z_mc**2, z_mc.diff(), z_mc.diff()**2], axis=1)
    all_mc.fillna(value=0, inplace=True)
    return Design_Matrix(all_mc, sampling_freq=1/tr)

for sub in layout.get_subjects(scope='derivatives'):
    sess = 'd1'
    data = Brain_Data([x for x in layout.get(subject=sub, session=sess, scope='derivative', suffix='bold', extension='nii.gz', return_type='file') if 'denoised' not in x][0])